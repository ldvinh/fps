﻿
namespace Assets.Scripts
{
    class Util
    {
        
    }
    public enum Team
    {
        TeamNone,
        TeamPlayer,
        TeamEnemy
    }
    public enum GameState
    {
        NewGame,
        Playing,
        Prepare,
        InitWave,
        Action,
        ShowDown,
        Over
    }
}
